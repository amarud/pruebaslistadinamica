/*
 * principal.cpp
 *
 *  Created on: 21/5/2018
 *      Author: brian
 */
#include "Lista.h"
#include "Cultivo.h"
#include <iostream>

int main(){
	Lista<Cultivo*>* cultivos = new Lista<Cultivo*>();

	Cultivo* cultivo1 = new Cultivo('A', 3, 2, 10, 4, 1);
	Cultivo* cultivo2 = new Cultivo('B', 4, 2, 10, 3, 1);
	Cultivo* cultivo3 = new Cultivo('C', 5, 2, 10, 2, 1);

	cultivos->agregar(cultivo1);
	cultivos->agregar(cultivo2);
	cultivos->agregar(cultivo3);


	Cultivo* cultivor1 = cultivos->remover(3);
	Cultivo* cultivor2 = cultivos->remover(2);
	Cultivo* cultivor3 = cultivos->remover(1);




	delete cultivor1;
	delete cultivor2;
	delete cultivor3;

	delete cultivos;

}
