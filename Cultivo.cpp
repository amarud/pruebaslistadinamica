/*
 * Cultivo.cpp
 *
 *  Created on: 13/05/2018
 *      Author: patron
 */


#include "Cultivo.h"

//Constructor con parametros, luego irá en .cpp
Cultivo::Cultivo(char nombre, ui costoSemilla, ui tiempoCosecha, ui rentabilidad, ui tiempoRecup, ui consumoRiego){
	/*if (!cultivo){
		throw std::string("No se ha podido definir el cultivo");
	}*/
	this->nombre = nombre;
	this->costoSemilla = costoSemilla;
	this->tiempoCosecha = tiempoCosecha;
	this->rentabilidad = rentabilidad;
	this->tiempoDeRecup = tiempoRecup;
	this->consumoRiego = consumoRiego;
}

ui Cultivo::verRentabilidad(){
	return this->rentabilidad;
}

ui Cultivo::verCostoSemilla(){
	return this->costoSemilla;
}

ui Cultivo::verNombre(){
	return this->nombre;
}


ui Cultivo::verTiempoDeRecup(){
	return this->tiempoDeRecup;
}

ui Cultivo::verTiempoDeCosecha(){
	return this->tiempoCosecha;
}

ui Cultivo::verConsumoRiego(){
	return this->consumoRiego;
}

void Cultivo::reducirTiempoCosecha(){
	this->tiempoCosecha--;
}

void Cultivo::reducirTiempoDeRecup(){
	this->tiempoDeRecup--;
}

Cultivo::~Cultivo(){
}
