/*
 * cultivo.h
 *
 *  Created on: 06/05/2018
 *      Author: patron
 */

#ifndef CULTIVO_H_
#define CULTIVO_H_

typedef unsigned int ui;

class Cultivo{
	private:
		char nombre;
		ui costoSemilla;
		ui tiempoCosecha;
		ui rentabilidad; //Ganancia al cosechar,luego de enviar a destino.
		ui tiempoDeRecup;
		ui consumoRiego; //Nuevo respecto del TP1

	public:


			Cultivo(char nombre, ui costoSemilla, ui tiempoCosecha, ui rentabilidad, ui tiempoRecup, ui consumoRiego);

			ui verRentabilidad();

			ui verCostoSemilla();

			ui verNombre();

			ui verTiempoDeRecup();

			ui verTiempoDeCosecha();

			ui verConsumoRiego();

			void reducirTiempoCosecha();

			void reducirTiempoDeRecup();

			~Cultivo();

};







#endif /* CULTIVO_H_ */
